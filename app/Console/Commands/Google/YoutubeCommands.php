<?php

namespace App\Console\Commands\Google;

use App\Services\Google\Youtube\Youtube;
use Illuminate\Console\Command;

class YoutubeCommands extends Command {

    private $youtube;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */


    protected $signature = 'google:youtube';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update data from youtube data API V3 and youtube Analytics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->youtube = new Youtube();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {

        $this->line("\nStart updating YouTube channels data...");
        try {
            $this->youtube->updateYoutubeChannelsDB();
            $this->line("Updated all users successfully.");
        } catch (\Exception $e) {
            $this->error("Error updating YouTube channels: " . $e->getMessage() . "\n");
        }

        return 0;
    }
}
