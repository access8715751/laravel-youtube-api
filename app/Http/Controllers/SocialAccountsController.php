<?php

namespace App\Http\Controllers;

use App\Services\KPI;
use Illuminate\Http\RedirectResponse;

class SocialAccountsController extends Controller {
    
    /**
     * @throws \Exception
     */
    public function auth($provider,$service = null): RedirectResponse {
        return KPI::getDriver($provider)->authRedirect($service);
    }

    /**
     * @throws \Exception
     */
    public function authCallback($provider,$service = null): array | string {
        return KPI::getDriver($provider)->authCB($service);
    }
}
