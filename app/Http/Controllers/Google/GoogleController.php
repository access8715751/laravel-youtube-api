<?php

namespace App\Http\Controllers\Google;

use App\Http\Controllers\Controller;
use App\Services\Google\Google;
use App\Services\Google\Youtube\Youtube;
use Exception;
use Illuminate\Http\Request;


class GoogleController extends Controller {

    private Google $google;
    private Youtube $youtube;

    public function __construct() {
        $this->google = new Google();
        $this->youtube = $this->google->service('youtube');
    }


    public function getAnalyticsRange(Request $request) {

        $data = $request->all();

        try {
            [$channel, $start, $end] = [$data['channel'], optional($data)['start'], optional($data)['end']];
            return $this->youtube->analyticRangeDB($channel, $start, $end);
        } catch (Exception $e) {
            return response()->json(['error' => 'An error occurred while processing your request.'], 500);
        }
    }

    public function getVideosRange(Request $request) {

        $data = $request->all();

        try {
            [$channel, $start, $end] = [$data['channel'], optional($data)['start'], optional($data)['end']];
            return $this->youtube->videosRangeDB($channel, $start, $end);
        } catch (Exception $e) {
            return response()->json(['error' => 'An error occurred while processing your request.'], 500);
        }
    }

    public function getChannelDetails(Request $request) {

        $data = $request->all();

        try {
            $channel = optional($data)['channel'];
            return $this->youtube->getChannelDetailsDB($channel);
        } catch (Exception $e) {
            return response()->json(['error' => 'An error occurred while processing your request.'], 500);
        }
    }


    public function getYoutubeAll(Request $request) {

        $data = $request->all();

        try {
            [$channel, $start, $end] = [$data['channel'], optional($data)['start'], optional($data)['end']];
            return $this->youtube->getYoutubeChannelDB($channel, $start, $end);
        } catch (Exception $e) {
            return response()->json(['error' => 'An error occurred while processing your request.'], 500);
        }
    }

    public function insightsDaysToMonths(Request $request) {
        $insights = json_decode($request->getContent(), false);
        try {
            return $this->youtube->convertDaysToMonths($insights);
        } catch (Exception $e) {
            return response()->json(['error' => 'An error occurred while processing your request.'], 500);
        }
    }
}
