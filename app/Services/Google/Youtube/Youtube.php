<?php

namespace App\Services\Google\Youtube;

use App\Models\Google\YoutubeChannel;
use App\Models\Google\YoutubeInsight;
use App\Models\Google\YoutubeVideo;
use App\Services\Google\Google;
use App\Services\Google\Youtube\APIFetching\YoutubeAnalyticsAPI;
use App\Services\Google\Youtube\APIFetching\YoutubeDataAPI;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Youtube {

    use YoutubeDataAPI;
    use YoutubeAnalyticsAPI;

    private mixed $apiKey;
    private string $youtubeEndPoint;
    private string $Token;

    public function __construct() {
        $this->apiKey = config('services.youtube.api_key');
        $this->youtubeEndPoint = config('services.youtube.endpoint');
    }



    /** 
     * Category : data fetch 
     */

    //This function will help us get channel details and all videos/shorts/lives in a channel
    function fetchChannelDataAPI($token): ?object {
        $channelData = $this->getChannelInfo($token);
        $channelID = $channelData->items[0]->id ?? null; // this is needed if there is no id
        return $channelID ? (object) ['channel' => $channelData, 'videos' => $this->getAllVideosFiltered($channelID)] : null;
    }


    function analyticRangeDB($channel_id, $startDate, $endDate) {

        (empty($endDate) || $endDate == 'today') && $endDate = date('Y-m-d');

        $channel = YoutubeChannel::where('channelIdentifier', $channel_id)->first();
        if (empty($channel)) return response()->json(['error' => 'Channel not found.'], 404);

        $query = $channel->youtubeInsights();

        isset($startDate) && $query = $query->whereBetween('day', [$startDate, $endDate]);

        $insights = $query->orderBy('day', 'asc')->get()
            ->map(function ($insight) {
                $insightArray = $insight->toArray();
                unset($insightArray['youtube_channel_id'], $insightArray['id']);
                return (object) $insightArray;
            })
            ->toArray();

        return $insights;
    }





    function videosRangeDB($channel_id, $startDate, $endDate) {

        (empty($endDate) || $endDate == 'today') && $endDate = date('Y-m-d');

        $channel = YoutubeChannel::where('channelIdentifier', $channel_id)->first();
        if (empty($channel)) return response()->json(['error' => 'Channel not found.'], 404);

        $query = $channel->youtubeVideos();

        isset($startDate) && $query = $query->whereBetween('publishedAt', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);

        $videos = $query->orderBy('publishedAt', 'asc')->get()
            ->map(function ($insight) {
                $videoArray = $insight->toArray();
                unset($videoArray['youtube_channel_id'], $videoArray['id']);
                return (object) $videoArray;
            })
            ->toArray();

        return $videos;
    }

    function convertDaysToMonths($insights) {
        if (empty($insights)) return response()->json(['error' => 'data analytics not found.'], 404);
        return $this->DaysToMonths($insights);
    }

    function getChannelDetailsDB($channel_id) {

        $channel = YoutubeChannel::where('channelIdentifier', $channel_id)->first();
        if (empty($channel)) return response()->json(['error' => 'Channel not found.'], 404);
        return (object) ['info' => $channel->channel_info, 'details' => $channel->basic_statistics_info];
    }

    function getYoutubeChannelDB($channel_id, $startDate, $endDate) {

        (empty($endDate) || $endDate == 'today') && $endDate = date('Y-m-d');
        $channel = YoutubeChannel::where('channelIdentifier', $channel_id)->first();

        if (empty($channel)) return response()->json(['error' => 'Channel not found.'], 404);

        $videos = $channel->youtubeVideos();
        $analytics = $channel->youtubeInsights();

        if (isset($startDate)) {
            $videos = $videos->whereBetween('publishedAt', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            $analytics = $analytics->whereBetween('day', [$startDate, $endDate]);
        }

        $videos = $videos->orderBy('publishedAt', 'asc')->get()
            ->map(function ($insight) {
                $videoArray = $insight->toArray();
                unset($videoArray['youtube_channel_id'], $videoArray['id']);
                return (object) $videoArray;
            })
            ->toArray();

        $insights = $analytics->orderBy('day', 'asc')->get()
            ->map(function ($insight) {
                $insightArray = $insight->toArray();
                unset($insightArray['youtube_channel_id'], $insightArray['id']);
                return (object) $insightArray;
            })
            ->toArray();

        return (object) [
            'info' => $channel->channel_info,
            'details' => $channel->basic_statistics_info,
            'videos' => $videos,
            'insights' => $insights
        ];
    }

    /** 
     * Category : database updates 
     */

    public function updateYoutubeChannelsDB(): string {
        YoutubeChannel::all()->each(function ($channel) {

            $channel_id = $channel->id;
            $access_token = Google::getGoogleAccessToken($channel->refreshToken);
            // This is general data using youtube Data API V3
            $channelData =  $this->fetchChannelDataAPI($access_token);
            if (!$channelData) {
                echo "Account with name '" . $channel->channel_info->title . "' access token has expired. \n";
                return;
            }
            $channelInfo = $channelData->channel->items[0];
            $insightsTable = $channel->YoutubeInsights;
            $channelInfoDetails = $channelInfo->snippet;
            // This is youtube analytics filtering function
            $update = $insightsTable->all() && Carbon::now()->diffInDays($insightsTable[0]->day) <= 3;
            $channelAnalytics = $this->AnalyticsDefault($access_token, $update);
            $videoCreationDate = preg_replace('/T|Z/', ' ', $channelInfoDetails->publishedAt); 
            $channelDataNeededForAnalytics = [$channel->id, $videoCreationDate];
            $this->updateYoutubeVideosDB($channelData->videos, $channel_id);


            $basic_statistics = $channelInfo->statistics;

            if ($channelAnalytics->rows) {
                $this->updateYoutubeInsightsDB($channelDataNeededForAnalytics, $channelAnalytics->rows, $update);
                $subs_ranking = $this->analyticsCountrySubs($access_token)->rows;
                $subs_ranking = array_combine(array_column($subs_ranking, 0), array_column($subs_ranking, 1));
                $basic_statistics->country_subs_ranking = $subs_ranking;
            }


            unset($basic_statistics->hiddenSubscriberCount);

            $channelAttributes = [
                'channel_info' => $channelInfoDetails,
                'basic_statistics_info' => $basic_statistics,
            ];

            YoutubeChannel::updateOrCreate(
                ['id' => $channel_id],
                $channelAttributes
            );
        });

        return 'update successful';
    }


    private function updateYoutubeVideosDB($videosExtracted, $id) {

        if (!$videosExtracted) return;

        foreach ($videosExtracted as $key => $value) {
            if (!$value) continue;
            foreach ($value as $element) {

                $videoContent = $element->snippet;
                $videoAttributes = [
                    'video_id' => $element->id,
                    'youtube_channel_id' => $id,
                    'type_video' => $key,
                    'category_id' => $videoContent->categoryId,
                    'title' => $videoContent->title,
                    'thumbnail' => $videoContent->thumbnails->high->url,
                    'description' => $videoContent->description ?? null,
                    'publishedAt' => Carbon::parse($videoContent->publishedAt)->format('Y-m-d H:i:s'),
                    'statistics' => $element->statistics,
                    'video_time' => $element->contentDetails->duration,
                    'tags' => optional($videoContent)->tags,
                ];

                YoutubeVideo::updateOrCreate(
                    ['video_id' => $element->id],
                    $videoAttributes
                );

                Log::info("\n Updated or added video $videoContent->title");
            }
        }
    }



    private function updateYoutubeInsightsDB($channelDataNeededForAnalytics, $analyticsExtracted, $update) {

        $attributeNames = [
            'day',
            'views',
            'redViews',
            'likes',
            'dislikes',
            'videosAddedToPlaylists',
            'videosRemovedFromPlaylists',
            'shares',
            'estimatedMinutesWatched',
            'estimatedRedMinutesWatched',
            'averageViewDuration',
            'averageViewPercentage',
            'annotationClickThroughRate',
            'annotationCloseRate',
            'annotationImpressions',
            'annotationClickableImpressions',
            'annotationClosableImpressions',
            'annotationClicks',
            'annotationCloses',
            'cardClickRate',
            'cardTeaserClickRate',
            'cardImpressions',
            'cardTeaserImpressions',
            'cardClicks',
            'cardTeaserClicks',
        ];

        [$id, $channelCreationDate] = $channelDataNeededForAnalytics;
        $analyticsExtracted = array_map(function ($element) use ($id, $attributeNames) {
            $analyticAttributes = array_combine($attributeNames, $element);
            $analyticAttributes['youtube_channel_id'] = $id;
            return $analyticAttributes;
        }, $analyticsExtracted);

        if ($update) {
            foreach ($analyticsExtracted as $analyticAttributes) {
                YoutubeInsight::updateOrCreate(
                    ['day' => $analyticAttributes['day'], 'youtube_channel_id' => $analyticAttributes['youtube_channel_id']],
                    $analyticAttributes
                );
            }
        } else {
            $defaultDate = date('Y-m-d', strtotime('-2 years'));
            if ($channelCreationDate > $defaultDate) {
                $analyticsExtracted = array_filter($analyticsExtracted, function ($element) use ($channelCreationDate) {
                    return $element['day'] >= $channelCreationDate;
                });
            }
            YoutubeInsight::insert($analyticsExtracted);
        }
    }
}
