<?php

namespace App\Services\Google\Youtube\APIFetching;

use DateTime;
use Illuminate\Support\Facades\Http;

trait YoutubeAnalyticsAPI {
    /*
        * The date should be Y-m-d format
    */
    private function AnalyticsRange($access_token, $startDate, $endDate) {
        $metrics = 'views,redViews,likes,dislikes,videosAddedToPlaylists,videosRemovedFromPlaylists,shares,estimatedMinutesWatched,estimatedRedMinutesWatched,averageViewDuration,averageViewPercentage,annotationClickThroughRate,annotationCloseRate,annotationImpressions,annotationClickableImpressions,annotationClosableImpressions,annotationClicks,annotationCloses,cardClickRate,cardTeaserClickRate,cardImpressions,cardTeaserImpressions,cardClicks,cardTeaserClicks';
        $dimensions = "day";
        $sort = "-day";
        $ids = "channel==MINE";
        $url = "https://youtubeanalytics.googleapis.com/v2/reports?"
            . "metrics=$metrics&dimensions=$dimensions"
            . "&sort=$sort&ids=$ids&startDate=$startDate&endDate=$endDate"
            . "&access_token=$access_token";

        $body = json_decode(Http::get($url));
        return $body;
    }

    /*
        * The date should be Y-m-d format
    */
    private function AnalyticsDefault($access_token, $update = false) {

        $startDate = $update ? date('Y-m-d', strtotime('-1 day')) : date('Y-m-d', strtotime('-2 years'));
        $endDate = date('Y-m-d');

        return $this->AnalyticsRange($access_token, $startDate, $endDate);
    }

    private function AnalyticsCountrySubs($access_token) {
        $startDate = date('Y-m-d', strtotime('-2 years'));
        $endDate = date('Y-m-d');
        $metrics = "subscribersGained";
        $dimensions = "country";
        $sort = "-subscribersGained";
        $url = "https://youtubeanalytics.googleapis.com/v2/reports?"
            . "dimensions=$dimensions&sort=$sort&metrics=$metrics"
            . "&ids=channel==MINE&startDate=$startDate&endDate=$endDate"
            . "&access_token=$access_token";
        $body = json_decode(Http::get($url));
        return $body;
    }

    public function DaysToMonths($data) {

        $aggregatedData = [];

        foreach ($data as $item) {
            $date = DateTime::createFromFormat('Y-m-d', $item->day);
            $monthYear = $date->format('Y-m');

            if (!isset($aggregatedData[$monthYear])) {
                $aggregatedData[$monthYear] = (object)[
                    "month" => $monthYear,
                    "views" => 0,
                    "redViews" => 0,
                    "likes" => 0,
                    "dislikes" => 0,
                    "videosAddedToPlaylists" => 0,
                    "videosRemovedFromPlaylists" => 0,
                    "shares" => 0,
                    "estimatedMinutesWatched" => 0,
                    "estimatedRedMinutesWatched" => 0,
                    "averageViewDuration" => [],
                    "averageViewPercentage" => [],
                    "annotationClickThroughRate" => 0,
                    "annotationCloseRate" => 0,
                    "annotationImpressions" => 0,
                    "annotationClickableImpressions" => 0,
                    "annotationClosableImpressions" => 0,
                    "annotationClicks" => 0,
                    "annotationCloses" => 0,
                    "cardClickRate" => 0,
                    "cardTeaserClickRate" => 0,
                    "cardImpressions" => 0,
                    "cardTeaserImpressions" => 0,
                    "cardClicks" => 0,
                    "cardTeaserClicks" => 0,
                ];
            }

            $aggregatedData[$monthYear]->views += $item->views;
            $aggregatedData[$monthYear]->redViews += $item->redViews;
            $aggregatedData[$monthYear]->likes += $item->likes;
            $aggregatedData[$monthYear]->dislikes += $item->dislikes;
            $aggregatedData[$monthYear]->videosAddedToPlaylists += $item->videosAddedToPlaylists;
            $aggregatedData[$monthYear]->videosRemovedFromPlaylists += $item->videosRemovedFromPlaylists;
            $aggregatedData[$monthYear]->shares += $item->shares;
            $aggregatedData[$monthYear]->estimatedMinutesWatched += $item->estimatedMinutesWatched;
            $aggregatedData[$monthYear]->estimatedRedMinutesWatched += $item->estimatedRedMinutesWatched;
            $aggregatedData[$monthYear]->averageViewDuration[] = $item->averageViewDuration;
            $aggregatedData[$monthYear]->averageViewPercentage[] = $item->averageViewPercentage;
            $aggregatedData[$monthYear]->annotationClickThroughRate += $item->annotationClickThroughRate;
            $aggregatedData[$monthYear]->annotationCloseRate += $item->annotationCloseRate;
            $aggregatedData[$monthYear]->annotationImpressions += $item->annotationImpressions;
            $aggregatedData[$monthYear]->annotationClickableImpressions += $item->annotationClickableImpressions;
            $aggregatedData[$monthYear]->annotationClosableImpressions += $item->annotationClosableImpressions;
            $aggregatedData[$monthYear]->annotationClicks += $item->annotationClicks;
            $aggregatedData[$monthYear]->annotationCloses += $item->annotationCloses;
            $aggregatedData[$monthYear]->cardClickRate += $item->cardClickRate;
            $aggregatedData[$monthYear]->cardTeaserClickRate += $item->cardTeaserClickRate;
            $aggregatedData[$monthYear]->cardImpressions += $item->cardImpressions;
            $aggregatedData[$monthYear]->cardTeaserImpressions += $item->cardTeaserImpressions;
            $aggregatedData[$monthYear]->cardClicks += $item->cardClicks;
            $aggregatedData[$monthYear]->cardTeaserClicks += $item->cardTeaserClicks;
        }

        $analyticsByMonth = array_map(function ($element) {
            $arrayViewDuration = $element->averageViewDuration;
            $arrayViewPercentage = $element->averageViewPercentage;
            $element->averageViewDuration = round(array_sum($arrayViewDuration) / count($arrayViewDuration), 2);
            $element->averageViewPercentage = round(array_sum($arrayViewPercentage) / count($arrayViewPercentage), 2);
            return $element;
        }, array_values($aggregatedData));

        return $analyticsByMonth;
    }
}
