<?php

namespace App\Services\Google;

use App\Models\Google\GoogleUser;
use App\Models\Google\YoutubeChannel;
use App\Services\Google\Auth;
use App\Services\Google\Youtube\Youtube;
use App\Services\KPI;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;

class Google extends KPI {

    use Auth;

    private Youtube $youtube;

    function __construct() {
        $this->youtube = new Youtube();
    }

    public function Service($serviceChoice): Youtube {
        return match ($serviceChoice) {
            'youtube' => $this->youtube
        };
    }

    //TODO Make a fusion with account creation table in database
    //After login/registration this function is called and it serves to get google account data so we can finish registion easily
    private function registerFlow($encryptedPayload) {
        $payload = json_decode(Crypt::decryptString($encryptedPayload))->payload;
        $user = $payload->user;
        //with the user information we gonna include the refresh token too
        $refresh_token = $payload->refreshToken;
        try {
            GoogleUser::create([
                'userId' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'refreshToken' => $refresh_token,
                'thumbnailURL' => $user->picture,
            ]);
        } catch (QueryException $e) {
            $errorCode = $e->getCode();
            $errorMessage = $e->getMessage();
            return match (intval($errorCode)) {
                23000 => 'Error : The user already exists in the database',
                42000 => 'Error : Syntax error or access violation',
                default => "newError $errorMessage",
            };
        }

        return 'Google user is registred!';
        //TODO Register should be stateless but login and confirmation via email should not be stateless
    }

    /*
        This function treats all possibilities for adding a youtube channel to a user account (google for now).
        * the model and relationship works in a way that : 
            - Youtube channel cannot be linked to the same account twice
            - if Youtube channel already exist in the database it will not create a new one, instead it will just reference it to the account
        * You can add multiple youtube accounts to a multiple google accounts even if a youtube channel is shared with multiple managers
    */
    private function registerYoutubeChannel($encryptedPayload) {
        $payload = json_decode(Crypt::decryptString($encryptedPayload))->payload;
        [$access_token, $refresh_token] = [$payload->token, $payload->refreshToken];
        $response = $this->Service('youtube')->getChannelInfo($access_token);


        if (!property_exists($response, 'items')) return "Permissions are not enough to make this request";

        $channel = optional($response)->items[0];
        if (!isset($channel)) return "Sorry, no youtube channel for this account";

        try {
            //TODO Hassen with user session get username id and replace $user
            $user = GoogleUser::skip(0)->take(1)->first();
            $channelID = $channel->id;
            // Searching for youtubeChannel so we can check associate already existing youtubeChannel from another account instead of recreating the same thing
            $existingYoutubeChannel = YoutubeChannel::where('channelIdentifier', $channelID)->first();
            //check if channel already exist in the database
            if ($existingYoutubeChannel) {
                //i know i can use $existingYoutubeChannel->googleUsers()->sync([$user->userId], false) but i explicity want it to throw errors by changing the join table constraint
                $existingYoutubeChannel->googleUsers()->attach($user->id);
            } else {
                $youtubeChannel = YoutubeChannel::create([
                    'channelIdentifier' => $channelID,
                    'channel_info' => $channel->snippet,
                    'basic_statistics_info' => $channel->statistics,
                    'refreshToken' => $refresh_token,
                ]);
                $user->youtubeChannels()->attach($youtubeChannel);
            }
        } catch (QueryException $e) {
            $errorCode = $e->getCode();
            $errorMessage = $e->getMessage();
            return match (intval($errorCode)) {
                23000 => 'Error : Youtube channel already linked with the user!',
                42000 => 'Error : Syntax error or access violation',
                default => "newError $errorMessage",
            };
        }

        return 'Youtube channel added successfully!';
    }


    function getYoutubeChannelsDB(): Collection {
        // This will get all youtube channels registred in the database
        $channels = YoutubeChannel::all();
        // This filtering will take only necessary elements like name and access_token for each user.
        return $channels->map(fn ($channel) => (object) [
            'channelIdentifier' => $channel->channelIdentifier,
            'access_token' =>  $this->getGoogleAccessToken($channel->refreshToken)
        ]);
    }

    //TODO will need to find a way to make this private (security) maybe by using protected | i don't think static is correct
    //This function give an access_token from a refresh_token which is a permanent key linked with your api key so no one can use except you.
    static function getGoogleAccessToken($refresh_token) {
        $response = Http::asForm()->post('https://oauth2.googleapis.com/token', [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
            'client_id' => config('services.google.client_id'),
            'client_secret' => config('services.google.client_secret'),
        ]);

        return $response->successful() ? $response->json()['access_token'] : 'Cannot retrieve Token';
    }
}
