<?php

namespace App\Services\Google;

use Illuminate\Support\Facades\Crypt;
use Laravel\Socialite\Facades\Socialite;
trait Auth {
    
    public function authRedirect($service) {
        $googlepath = 'https://www.googleapis.com/auth';
        
        $scopes = match ($service) {
            'youtube' => [
                "$googlepath/yt-analytics.readonly",
                "$googlepath/youtube.readonly",
            ],
            //this is scopes for googleauth
            default => [
                "$googlepath/userinfo.profile",
                "$googlepath/userinfo.email",
            ]
        };

        /** @disregard P1009 Undefined type */
        $d = Socialite::driver('google')->stateless();
        $secure_url = secure_url(route('api.authCallback', ['provider' => "google", 'service' => $service]));
        $d->redirectUrl($secure_url);
        $d->scopes($scopes);
        //This is needed so we can get refreshToken instead of getting null
        $d->with(['access_type' => 'offline', 'prompt' => 'consent select_account']);
        return $d->redirect();
    }

    public function authCB($service) {
        /** @disregard P1009 Undefined type */        
        $d = Socialite::driver('google')->stateless();
        $secure_url = secure_url(route('api.authCallback', ['provider' => "google", 'service' => $service]));
        $d->redirectUrl($secure_url);
        $p = ['type' => 'google', 'payload' => $d->user()];
        
        $data = Crypt::encryptString(json_encode($p));
        
        return match ($service) {
            //TODO need to get user session or cookie id to save with youtube
            'youtube' => $this->registerYoutubeChannel($data),
            // this is google account registration
            default => $this->registerFlow($data),
        };
    }
}
