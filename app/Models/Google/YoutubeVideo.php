<?php

namespace App\Models\Google;

use App\Models\Google\YoutubeChannel;
use App\Utils\Casts\ToObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YoutubeVideo extends Model {
    use HasFactory;
    

    public $timestamps = false;

    protected $fillable = [
        'youtube_channel_id',
        'video_id',
        'type_video',
        'category_id',
        'tags',
        'title',
        'thumbnail',
        'description',
        'publishedAt',
        'statistics',
        'video_time',
        'country',
    ];

    
    protected $casts = [
        'statistics' => ToObject::class,
        'tags' => 'array'
    ];


    public function youtubeChannel() {
        return $this->belongsTo(YoutubeChannel::class);
    }
}
