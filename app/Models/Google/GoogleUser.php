<?php

namespace App\Models\Google;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoogleUser extends Model {

    use HasFactory;



    public $timestamps = false;


    protected $fillable = [
        'google_primary_id',
        'userId',
        'name',
        'email',
        'thumbnailURL',
        'refreshToken'
    ];

    protected $casts = [
        'userId' => 'string',
    ];



    //this is used to get nested relation table youtubeChannels without using Model::with('youtubeChannels')->get();
    protected static function booted() {
        static::addGlobalScope('youtubeChannels', function (Builder $builder) {
            $builder->with('youtubeChannels');
        });
    }

    public function youtubeChannels() {
        // googleid is the reference of foreign key in YoutubeChannel to primary key userId in GoogleUser
        return $this->belongsToMany(YoutubeChannel::class);
            
    }
}
