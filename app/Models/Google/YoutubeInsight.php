<?php

namespace App\Models\Google;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YoutubeInsight extends Model {
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'day',
        'views',
        'redViews',
        'likes',
        'dislikes',
        'videosAddedToPlaylists',
        'videosRemovedFromPlaylists',
        'shares',
        'estimatedMinutesWatched',
        'estimatedRedMinutesWatched',
        'averageViewDuration',
        'averageViewPercentage',
        'annotationClickThroughRate',
        'annotationCloseRate',
        'annotationImpressions',
        'annotationClickableImpressions',
        'annotationClosableImpressions',
        'annotationClicks',
        'annotationCloses',
        'cardClickRate',
        'cardTeaserClickRate',
        'cardImpressions',
        'cardTeaserImpressions',
        'cardClicks',
        'cardTeaserClicks',
        'youtube_channel_id'
    ];


    public function youtubeChannel() {
        return $this->belongsTo(YoutubeChannel::class);
    }
}
