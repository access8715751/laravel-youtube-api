<?php

namespace App\Models\Google;

use App\Utils\Casts\ToObject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YoutubeChannel extends Model {
    
    use HasFactory;
        

    //this disables created_at and modified_at
    public $timestamps = false;


    protected $fillable = [
        'channelIdentifier',
        'refreshToken',
        'channel_info',
        'basic_statistics_info',
    ];

    //Instad of getting JSON data as string it convert it automatically into object 
    //If you have JSON data in channel_info for example in your database you can test using YoutubeChannel::first()->channel_info
    protected $casts = [
        'channel_info' => ToObject::class,
        'basic_statistics_info' => ToObject::class,
    ];


    protected static function booted() {
        static::addGlobalScope('youtubeVideos', function (Builder $builder) {
            $builder->with('youtubeVideos');
        });

        static::addGlobalScope('YoutubeInsights', function (Builder $builder) {
            $builder->with('YoutubeInsights');
        });
    }

    
    public function googleUsers(){
        // userId is the reference of primary key in GoogleUser to foreign key google_user_id in YoutubeChannel
        return $this->belongsToMany(GoogleUser::class);
    }

    public function youtubeVideos(){
        return $this->hasMany(YoutubeVideo::class);
    }

    public function youtubeInsights(){
        return $this->hasMany(YoutubeInsight::class);
    }
}
