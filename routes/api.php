<?php

use App\Http\Controllers\Google\GoogleController;
use App\Http\Controllers\SocialAccountsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//FIXME hassen need to find a solution for this since it can make problems for the other providers since they need to use //callback too instead of /callback
//The service? variable means that we don't need to type service and the request still works, this will be replaced with the old socialmedia path
Route::group(['as' => 'api.'], function () {
    Route::post('/google/youtube/analytics/month', [GoogleController::class, 'insightsDaysToMonths']);
    Route::post('/google/youtube/analytics', [GoogleController::class, 'getAnalyticsRange']);
    Route::post('/google/youtube/videos', [GoogleController::class, 'getVideosRange']);
    Route::post('/google/youtube/details', [GoogleController::class, 'getChannelDetails']);
    Route::post('/google/youtube/all', [GoogleController::class, 'getYoutubeAll']);
    Route::get('social-accounts/auth/{provider}/{service?}', [SocialAccountsController::class, 'auth']);
    Route::get('social-accounts/auth/{provider}/{service?}/callback', [SocialAccountsController::class, 'authCallback'])->where('service', '.*')->name('authCallback');
});


// TODO Implement login and logout using Google (Hassen or Chedli)