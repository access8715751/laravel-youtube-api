<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoogleUserYoutubeChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_user_youtube_channel', function (Blueprint $table) {
            $table->id();
            $table->string('google_user_id');
            $table->string('youtube_channel_id')->index(); // i am not sure if index is good for performance in this case but normally it is since i am searching with it;

            // Create a composite unique index on channelIdentifier and userId
            $table->unique(['google_user_id', 'youtube_channel_id'], 'unique_channel_google');
        });
    }

    /**
     * Reverse the migrations.<
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_user_youtube_channel');
    }
}
