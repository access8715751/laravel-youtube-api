<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYoutubeInsightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youtube_insights', function (Blueprint $table) {
            $table->id();
            $table->string('youtube_channel_id');
            $table->date('day');
            $table->integer('views')->nullable();
            $table->integer('redViews')->nullable();
            $table->integer('likes')->nullable();
            $table->integer('dislikes')->nullable();
            $table->integer('videosAddedToPlaylists')->nullable();
            $table->integer('videosRemovedFromPlaylists')->nullable();
            $table->integer('shares')->nullable();
            $table->integer('estimatedMinutesWatched')->nullable();
            $table->integer('estimatedRedMinutesWatched')->nullable();
            $table->integer('averageViewDuration')->nullable();
            $table->float('averageViewPercentage')->nullable();
            $table->float('annotationClickThroughRate')->nullable();
            $table->float('annotationCloseRate')->nullable();
            $table->integer('annotationImpressions')->nullable();
            $table->integer('annotationClickableImpressions')->nullable();
            $table->integer('annotationClosableImpressions')->nullable();
            $table->integer('annotationClicks')->nullable();
            $table->integer('annotationCloses')->nullable();
            $table->float('cardClickRate')->nullable();
            $table->float('cardTeaserClickRate')->nullable();
            $table->integer('cardImpressions')->nullable();
            $table->integer('cardTeaserImpressions')->nullable();
            $table->integer('cardClicks')->nullable();
            $table->integer('cardTeaserClicks')->nullable();
            $table->unique(['youtube_channel_id', 'day']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youtube_analytics');
    }
}
