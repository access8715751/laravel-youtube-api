<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYoutubeChannelsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
            Schema::create('youtube_channels', function (Blueprint $table) {
                $table->id(); // primary key
                $table->string('channelIdentifier')->unique();
                $table->string('refreshToken');  // this refresh token is using the google API to generate an access_token (security)
                
                $table->json('channel_info'); //contains channel basic informations
                $table->json('basic_statistics_info'); //contains all viewCounts,sub counts and total videoCount
                
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('youtube_channels');
    }
}
