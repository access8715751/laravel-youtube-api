<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYoutubeVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youtube_videos', function (Blueprint $table) {
            $table->id();
            $table->integer('youtube_channel_id');
            $table->enum('type_video', ['VIDEO', 'SHORT', 'LIVE']);
            $table->integer("category_id");
            $table->string('video_id')->unique();
            $table->string('title');
            $table->string('thumbnail');
            $table->text('description')->nullable();
            $table->string('country')->nullable();
            $table->timestamp('publishedAt');
            $table->json('statistics');
            $table->string('video_time',11);
            $table->json('tags')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youtube_videos');
    }
}
