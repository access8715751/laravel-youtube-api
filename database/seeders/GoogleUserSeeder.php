<?php

namespace Database\Seeders;

use App\Models\Google\GoogleUser;
use App\Models\Google\YoutubeChannel;
use App\Models\Google\YoutubeInsight;
use Illuminate\Database\Seeder;

class GoogleUserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void {
        $user1 = GoogleUser::factory()->create();
        $user2 = GoogleUser::factory()->create();

        $channel1 = YoutubeChannel::factory()->hasYoutubeVideos(2)->create();
        $channel2 = YoutubeChannel::factory()->hasYoutubeVideos(1)->create();
        $channel3 = YoutubeChannel::factory()->hasYoutubeVideos(1)->create();
        $this->generateInsightsForDateRange('2023-01-01', '2023-01-10', $channel1->id);
        $this->generateInsightsForDateRange('2022-01-01', '2022-05-10', $channel2->id);
        $this->generateInsightsForDateRange('2023-01-01', '2023-09-10', $channel3->id);

        // Seed a GoogleUser with 1 associated YouTube channel
        $user1->youtubeChannels()->attach([$channel1->id, $channel2->id]);

        // Seed another GoogleUser with 2 associated YouTube channels
        $user2->youtubeChannels()->attach([$channel2->id, $channel3->id]);
    }

    // Generate random insights by specifying data range
    private function generateInsightsForDateRange(string $startDate, string $endDate, int $youtubeChannelId): void {
     
        $startTimestamp = strtotime($startDate);
        $endTimestamp = strtotime($endDate);

        // Generate data for each day within the interval
        for ($currentTimestamp = $startTimestamp; $currentTimestamp <= $endTimestamp; $currentTimestamp += 86400) {
            $currentDate = date('Y-m-d', $currentTimestamp);
            YoutubeInsight::factory()->create([
                'day' => $currentDate,
                'youtube_channel_id' => $youtubeChannelId,
            ]);
        }
    }
}
