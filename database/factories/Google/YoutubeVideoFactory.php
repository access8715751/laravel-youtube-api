<?php

namespace Database\Factories\Google;

use App\Models\Google\YoutubeChannel;
use App\Models\Google\YoutubeVideo;
use Illuminate\Database\Eloquent\Factories\Factory;

class YoutubeVideoFactory extends Factory {


    /**
     * The name of the factory's corresponding model
     *
     * @var string
     */

    protected $model = YoutubeVideo::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {

        $categoryTable = [22,23,24];
        $typeTable = ["live","video","short"];
        


        return [
            'video_id' => $this->faker->unique()->randomNumber(8),
            'youtube_channel_id' => YoutubeChannel::factory(),
            'type_video' => $this->faker->randomElement($typeTable),
            'category_id' => $this->faker->randomElement($categoryTable),
            'tags' => $this->faker->words(3),
            'title' => $this->faker->sentence(),
            'thumbnail' => $this->faker->imageUrl(),
            'description' => $this->faker->paragraph(),
            'publishedAt' => $this->faker->dateTimeBetween('-1 year', 'now')->format('Y-m-d H:i:s'),
            'statistics' => [
                'likeCount' => '0',
                'viewCount' => '13',
                'commentCount' => '0',
                'favoriteCount' => '0'
            ],
            'video_time' => $this->faker->dateTimeBetween('-1 year', 'now')->format('Y-m-d')
        ];
    }
}
