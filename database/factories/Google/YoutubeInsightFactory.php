<?php

namespace Database\Factories\Google;

use App\Models\Google\YoutubeChannel;
use App\Models\Google\YoutubeInsight;
use Illuminate\Database\Eloquent\Factories\Factory;

class YoutubeInsightFactory extends Factory {
    /**
     * The name of the factory's corresponding model
     *
     * @var string
     */

    protected $model = YoutubeInsight::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array {

        // Generate data for each day within the interval
        return [
            'day' => $this->faker->date(),
            'youtube_channel_id' =>  YoutubeChannel::factory(),
            'views' => $this->faker->numberBetween(1,  1000000),
            'redViews' => $this->faker->numberBetween(0,  1000000),
            'likes' => $this->faker->numberBetween(0,  1000000),
            'dislikes' => $this->faker->numberBetween(0,  1000000),
            'videosAddedToPlaylists' => $this->faker->numberBetween(0,  1000000),
            'videosRemovedFromPlaylists' => $this->faker->numberBetween(0,  1000000),
            'shares' => $this->faker->numberBetween(0,  1000000),
            'estimatedMinutesWatched' => $this->faker->numberBetween(0,  1000000),
            'estimatedRedMinutesWatched' => $this->faker->numberBetween(0,  1000000),
            'averageViewDuration' =>  $this->faker->randomFloat(1,  0,  100),
            'averageViewPercentage' => $this->faker->randomFloat(2,  0,  100),
            'annotationClickThroughRate' => $this->faker->randomFloat(2,  0,  100),
            'annotationCloseRate' => $this->faker->randomFloat(2,  0,  100),
            'annotationImpressions' => $this->faker->numberBetween(0,  1000000),
            'annotationClickableImpressions' => $this->faker->numberBetween(0,  1000000),
            'annotationClosableImpressions' => $this->faker->numberBetween(0,  1000000),
            'annotationClicks' => $this->faker->numberBetween(0,  1000000),
            'annotationCloses' => $this->faker->numberBetween(0,  1000000),
            'cardClickRate' => $this->faker->randomFloat(2,  0,  100),
            'cardTeaserClickRate' => $this->faker->randomFloat(2,  0,  100),
            'cardImpressions' => $this->faker->numberBetween(0,  1000000),
            'cardTeaserImpressions' => $this->faker->numberBetween(0,  1000000),
            'cardClicks' => $this->faker->numberBetween(0,  1000000),
            'cardTeaserClicks' => $this->faker->numberBetween(0,  1000000),
        ];
    }
}
